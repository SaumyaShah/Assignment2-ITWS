#!/bin/bash
#df | tr -s " " | cut -d" " -f 1,5
n=$(df | wc -l)


for((i=2;i<=$n;i++))
do
	file=`df | tr -s " " | cut -d" " -f 1 | head -$i | tail -1`
	a=`df | tr -s " " | cut -d" " -f 5 | head -$i | tail -1`
	b=`df | tr -s " " | cut -d" " -f 5 | cut -d"%" -f 1 |head -$i | tail -1`
	if (($b<$1))
	then
		echo "OK, $file, $a"
	elif(($b>=$1 && $b<$2))
	then
		echo "WARNING, $file, $a"
	else
		echo "CRITICAL, $file, $a"
	fi
done




