#!/bin/bash
if [ "$#" -ne 1 ]
then 
echo "Please enter only 1 argument"
elif [ -f $1 ]
then
chmod u+x $1
else
echo "File does not exist"
fi
