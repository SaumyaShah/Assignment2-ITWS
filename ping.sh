#!/bin/bash
IFS=$'\n'
a=0
for i in `ping -c $1 google.com | perl -nle 'print scalar(localtime), " " , $_'`
do
	tput setaf $a
	if (($a<=6))
	then
		echo $i
		let a++
	else 
		a=0
	fi
done



